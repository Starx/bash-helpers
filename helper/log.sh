#!/bin/bash

# basic log fucntion
#
# The colors helper is needed to show colors
#
# @param msg_level
# @param message
#
log() {
	local msg_level=$1
	local msg="$2"

	# Parse Command Line Arguments
	local arg_shift_position=2
	local headless=false
	local log_to_stderr=true
	local no_newline=false
	local save_log_to_file=false
	local log_file=
	# Attempt to get the log prefix from the environment
	file_env 'LOG_PREFIX' ''
	log_prefix="$LOG_PREFIX"

	while [ "$#" -gt $arg_shift_position ]; do
	  # Dynamic variables based on arg_shift_position
	  id_var=$((arg_shift_position+1))
	  value_var=$((arg_shift_position+2))
	  arg_id=${!id_var}
	  arg_value=${!value_var}

	  case "$arg_id" in
	  	--headless)
			headless=true
			;;
		--stdout)
			log_to_stderr=false
			;;
	  	--chainable-log)
			no_newline=true
			;;
	  	--store-log)
			save_log_to_file=true
			;;
		--log-file)
			save_log_to_file=true # Because Duh!!!
			log_file=$arg_value
			;;
		--log-prefix)
			log_prefix="$arg_value"
			;;
		*)
			shift
			continue;
		;;
	  esac

	  # Shift the arguments as per required
	  case "$arg_id" in
	  	--headless|--chainable-log|--store-log|--stdout)
			shift
			;;
		--log-file|--log-prefix)
	  		shift
	  		shift
			;;
	  esac
	done
	local output_msg=false

	# Add the prefix if provided
	if [ ! -z "$log_prefix" ]; then
		msg="${log_prefix}${msg}"
	fi

	if [ "$log_level" == "suppress" ]; then
		return 0;
	elif [ "$log_level" == "verbose3" ]; then
		output_msg=true
	elif [ "$log_level" == "verbose2" ] && [ "$msg_level" != "verbose3" ]; then
		output_msg=true
	elif [ "$log_level" == "verbose" ] && [ "$msg_level" != "verbose3" ] && [ "$msg_level" != "verbose2" ]; then
		output_msg=true
	elif [ "$log_level" == "debug" ] && [[ "$msg_level" != verbose* ]]; then
		output_msg=true
	elif [ "$log_level" == "info" ] && [[ "$msg_level" != verbose* ]] && [ "$msg_level" != "debug" ]; then
		output_msg=true
	elif [ "$log_level" == "warn" ] && ( [[ "$msg_level" != verbose* ]] &&[ "$msg_level" != "debug" ] && [ "$msg_level" != "info" ] && [ "$msg_level" != "success" ] ); then
		output_msg=true
	elif [ "$log_level" == "error" ] && ( [ "$msg_level" == "error" ] || [ "$msg_level" == "fatal" ] ); then
		output_msg=true
	elif [ "$log_level" == "fatal" ] && [ "$msg_level" == "fatal" ]; then
		output_msg=true
	fi

	if ! $headless; then
		if [[ "$msg_level" == verbose* ]]; then
			msg="${BBlack}VERBOSE:\t${NC} ${BBlack}$msg${NC}"		
		elif [ "$msg_level" == "debug" ]; then
			msg="${BBlack}DEBUG:\t${NC} $msg"
		elif [ "$msg_level" == "info" ]; then
			msg="INFO:\t $msg"
		elif [ "$msg_level" == "success" ]; then
			msg="${BGreen}SUCCESS:\t${NC} $msg"
		elif [ "$msg_level" == "warn" ]; then
			msg="${Yellow}WARNING:\t${NC} $msg"
		elif [ "$msg_level" == "error" ]; then
			msg="${Red}ERROR:\t${NC} $msg"
		elif [ "$msg_level" == "fatal" ]; then		
			msg="${BRed}FATAL:\t${NC} $msg"
		fi
	else
		if [[ "$msg_level" == verbos* ]]; then
			msg="${BBlack}$msg${NC}"
		fi
	fi 

	local log_echo="echo -e"

	if $no_newline; then
		log_echo="$log_echo -n"
	fi

	if ! $headless; then
		log_echo="$log_echo `date +"%Y-%m-%d %T"`"
	fi	

	if $output_msg; then
		if $log_to_stderr; then
			# Output to stderr
			>&2 $log_echo "$msg"
		else
			$log_echo "$msg"
		fi
	fi
}

#
# Test method to test different level of log levels
# P.S. Small joke.
log_test() {
	echo "Thoughts of a guy walking up to a colleague to chat, who faints."
	log "verbose3" "Ohh! This guy again."
	log "verbose2" "He does not look good."
	log "verbose" "Meh!"
	log "debug" "Hey, man!"
	log "info" "How are you doing?"
	log "success" "Nice."
	log "info" "blah.. blah.. blah"
	log "warn" "Oh no! Are you ok? (about to faint)"
	log "error" "WTF! (he fell down)"
	log "fatal" "HELP!!!"
}

# Var to help check and source the script only once
starx_bash_helper_log_loaded=true
