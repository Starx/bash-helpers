#!/bin/bash

# If `sudo` does not exists like when running from MinGW, create a wrapper function
command -v sudo > /dev/null
if [ ! $? -eq 0 ]; then
	# Wrapper function: It's basically a eval function which is SUPER dangerous
	function sudo {	
		"$@";
	}
fi

# Var to help check and source the script only once
starx_bash_helper_sudo_loaded=true
