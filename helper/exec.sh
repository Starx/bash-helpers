#!/bin/bash

function execute_if_function_exists() {
	function_name="$1"
	shift
	function_exists "$function_name" && $function_name "$@"
}

function execute {
	local cmd="$1"
	file_env S2_EXEC_ENV "$exec_env"
	file_env S2_EXEC_REMOTE "$exec_target_remote"
	log "debug" "execute::S2_EXEC_ENV=$S2_EXEC_ENV"
	log "debug" "execute::S2_EXEC_REMOTE=$S2_EXEC_REMOTE"
	log "verbose" "execute::cmd"
	log "verbose"
	log "verbose" "$cmd"

	if [[ $S2_EXEC_ENV == "local" ]]; then
		execute_local "$cmd"
	elif [[ $S2_EXEC_ENV == "remote" ]]; then
		execute_remote "$cmd" $S2_EXEC_REMOTE
	fi
	return $?
}

function execute_local {
	local cmd="$1"
	bash << EOF
$cmd
EOF
	return $?
}

function execute_remote {
	local cmd="$1"
	local ssh_host="$2"
	ssh -T $ssh_host << EOF
$cmd
EOF
	return $?
}

# Var to help check and source the script only once
starx_bash_helper_exec_loaded=true
