#!/bin/bash

# Checks if public key exists
#
# @param key id / user id
# @param GPG home path (optional)
# @param GPG cmd (optional)
#
# @todo use return code rather than declare a var
check_if_key_exists() {
	local key_id="$1"
	local gpg_home="${2:-"$GNUPGHOME"}"
	local gpg_cmd="${3:-gpg}"

	$gpg_cmd --homedir "$gpg_home" -k "$key_id" > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		key_exists=true
	else
		key_exists=false
	fi
}

# Checks if secret/private key exists
#
# @param key id / user id
# @param GPG home path (optional)
# @param GPG cmd (optional)
#
# @todo use return code rather than declare a var
check_if_secret_key_exists() {
	local key_id="$1"
	local gpg_home="${2:-"$GNUPGHOME"}"
	local gpg_cmd="${3:-gpg}"

	$gpg_cmd --homedir "$gpg_home" -K "$key_id" > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		key_exists=true
	else
		key_exists=false
	fi
}

generate_unattended_key() {
	local user_id=$1
	local passphrase=$2
	local gpg_home="${3:-"$GNUPGHOME"}"
	local gpg_cmd="${4:-gpg}"

	local batch_file=$(mktemp)

	cat >"$batch_file" <<EOF
     %echo Generating a basic OpenPGP key
     Key-Type: RSA
     Key-Length: 4096
     Name-Email: ${user_id}
     Expire-Date: 2y
     Passphrase: ${passphrase}
     # Do a commit here, so that we can later print "done" :-)
     %commit
     %echo done
EOF
	$gpg_cmd \
		--homedir "$gpg_home" \
		--batch --gen-key "$batch_file" > /dev/null 2>&1
	generation_status=$?
	
	# Remove the file	
	rm -f "$batch_file"
	return $generation_status
}

change_key_passphrase_unattended() {
	local key_id="$1"
	local current_passphrase="$2"
	local new_passphrase="$3"
	local gpg_home=${4:-"$GNUPGHOME"}
	local gpg_cmd="${5:-gpg}"

	local batch_file=$(mktemp)
	local output_file=$(mktemp)

	cat >"$batch_file" <<EOF 
passwd
${current_passphrase}
${new_passphrase}
save
EOF

	# Edit the key
	$gpg_cmd \
		--homedir "$gpg_home" \
		--no-tty --pinentry-mode loopback \
		--command-fd 0 --status-fd 2 \
		--edit-key "$key_id" \
		< "$batch_file" \
		> "$output_file" 2>&1
	edit_status=$?

	if [ $edit_status -eq 0 ]; then
		# Exit code is 0 even if there was a bad passphrase error, so test for that
		cat "$output_file" | grep ": Bad passphrase" > /dev/null 2>&1
		if [ $? -eq 0 ]; then
			# If ": Bad passphrase" was found then there was a problem with passphrase
			# So, update the edit_status manually to represent the failure
			edit_status=1
			
			# Read the output file and output to stderr
			>&2 cat "$output_file"
		fi
	fi

	# Remove the temp files
	rm -f "$batch_file"
	rm -f "$output_file"

	# Respond with the status
	return $edit_status	
}

generate_unattended_key_revocation() {
	local key_id=$1
	local passphrase=$2
	local output_file_path=$3
	local gpg_home=${4:-"$GNUPGHOME"}
	local gpg_cmd="${5:-gpg}"

	local batch_file=$(mktemp)

	local code=1
	local reason="Compromised"

	cat >"$batch_file" <<EOF 
y
${code}
${reason}

y
y
EOF
	# Remove the file if it exists
	if [ -f "$output_file_path" ]; then
		rm -f "$output_file_path"
	fi

	# Generate the revoke
	$gpg_cmd \
		--homedir "$gpg_home" \
		--no-tty --pinentry-mode loopback \
		--passphrase "$passphrase" \
		--command-fd 0 --status-fd 2 \
		-a -o "$output_file_path" \
		--gen-revoke "$key_id" \
		< "$batch_file" \
		> /dev/null 2>&1
	generation_status=$?

	# Remove the file
	rm -f "$batch_file"

	# Respond with the status
	return $generation_status	
}

# Var to help check and source the script only once
starx_bash_helper_gpg_loaded=true