#!/bin/bash

# Trim functions

# Trims white spaces (leading and trailing)
trim2() {
	echo -e "${1}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'
}

# Trims white spaces (leading and trailing)
trim() {
    local var="$*"
    # remove leading whitespace characters
    var="${var#"${var%%[![:space:]]*}"}"
    # remove trailing whitespace characters
    var="${var%"${var##*[![:space:]]}"}"   
    echo -n "$var"
}

# Source: https://stackoverflow.com/a/45201229/295264
function mfcb { local val="$4"; "$1"; eval "$2[$3]=\$val;"; };
function val_ltrim { if [[ "$val" =~ ^[[:space:]]+ ]]; then val="${val:${#BASH_REMATCH[0]}}"; fi; };
function val_rtrim { if [[ "$val" =~ [[:space:]]+$ ]]; then val="${val:0:${#val}-${#BASH_REMATCH[0]}}"; fi; };
function val_trim { val_ltrim; val_rtrim; };

function explode_custom {
	local string="$1"
	local delimiter="$2"
	exploded_array=()
	readarray -c1 -C 'mfcb val_trim exploded_array' -td"$delimiter" <<<"$string$delimiter"; unset 'exploded_array[-1]';
}

function explode_ifs {
	local string="$1"
	local delimiter="$2"
	exploded_array=()
	IFS="$delimiter" read -r -a exploded_array <<< "$string"
}

function explode {
	explode_custom "$@"
}


# Ref: https://stackoverflow.com/a/17841619
function join_by { local d=${1-} f=${2-}; if shift 2; then printf %s "$f" "${@/#/$d}"; fi; }


# https://stackoverflow.com/a/2317171
function join_array {
	separator=")|(" # e.g. constructing regex, pray it does not contain %s
	foo=('foo bar' 'foo baz' 'bar baz')
	regex="$( printf "${separator}%s" "${foo[@]}" )"
	regex="${regex:${#separator}}"
	echo "${regex}"
# Prints: foo bar)|(foo baz)|(bar baz
}

# Var to help check and source the script only once
starx_bash_helper_string_loaded=true
