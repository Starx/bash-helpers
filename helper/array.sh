#!/bin/bash

function in_array {
	# Assume the first param in needle
	local value=$1 #needle
	
	# And assume that rest of the arguments are the array to test against

	# bump the first argument off the argument list
	shift
	# then all arguments as a collective arrawy
	local array=("$@") #haystack

	# Test if array contains the value

	if [[ " ${array[*]} " =~ " ${value} " ]]; then
		# EC 0 is true
		return 0	
	fi
	# EC 1 is not true (thus ~= false)
	return 1
}

# Var to help check and source the script only once
starx_bash_helper_color_loaded=true
