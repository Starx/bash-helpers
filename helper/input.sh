#!/bin/bash

# Ask ANY question and get a reply in Y or N form
# @param question
# @param default_value
#
function confirm {
	confirmed=false
	local question="$1"
	question="$question (Y/N)"
	local default_value=$2
	local default_provided=false
	if [[ $2 ]]; then
		default_provided=true
		question="$question [$default_value]"
	fi

	question="$question: "


	while true; do
		read -r -p "$(echo -e $question)" yn
	    case "$yn" in
	        [Yy]* ) 
				confirmed=true
				break 1;
				;;
	        [Nn]* ) 
				confirmed=false
				break 1;
				;;
	        ""|*)
				if [[ ! -z $yn ]]; then
					echo "'$yn' is not valid answer."
				fi

				if $default_provided; then
					log default "$FUNCNAME: Using default value: $default_value"
					confirmed=$default_value
					break;
				else
					log verbose3 "$yn is not a valid response."
					echo "Please answer in yes(Y) or no(N)."
				fi
			;;
	    esac
	done
	CONFIRMED=$confirmed
}


function read_input {
	local variable_name="$1"
	
	# Set a default value for argument if not passed
	local message="$2"
	if [ -z "$message" ]; then
		message="Input ${variable_name}"
	fi

	local default_value="$3"
	local default_provided=false

	# Set a default value for argument if not passed
	local read_multi_line=${4:-false}
	# Set a default value for argument if not passed
	local hidden=${5:-false}

	log debug "$FUNCNAME: variable_name: $variable_name"
	log debug "$FUNCNAME: message: $message"
	log debug "$FUNCNAME: default_value: $default_value"
	log debug "$FUNCNAME: read_multi_line: $read_multi_line"
	log debug "$FUNCNAME: hidden: $hidden"

	if [[ $3 ]]; then
		default_provided=true
		message="$message [$default_value]"
	fi

	# Suffix with ": "
	message="$message: "

	# Variable to capture the input
	local input

	# Read 
	echo -n "$message"
	if $read_multi_line; then
		# Read multi line input. Read until there is a blank line at the end
		# Ref: https://stackoverflow.com/a/20913871
		input=$(sed '/^$/q')
	else
		if $hidden; then
			read -s input
		else
			read input
		fi
	fi
	
	echo

	if [ -z "$input" ]; then
		input="$default_value"
	fi

	# Assigning input with printf
	# Ref: https://stackoverflow.com/a/55331060
	printf -v "$variable_name" '%s' "$input"
}

function read_input_if_not_defined {
	local variable_name="$1"
	local message="$2"
	local default_value="$3"
	local read_multi_line=${4:-false}
	local hidden=${5:-false}

	# Since Bash 4.3 (released 2014), the declare builtin has an option -n
	# for creating a variable which is a “name reference” to another variable
	declare -n ref="$variable_name"
	if [ -z "$ref" ]; then
		log debug "$FUNCNAME: $variable_name is not defined."
		read_input "$variable_name" "$message" "$default_value" $read_multi_line $hidden
	fi
}

# Var to help check and source the script only once
starx_bash_helper_input_loaded=true
