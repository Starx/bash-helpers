#!/bin/bash

# Check if function exists
function function_exists() {
    declare -f -F $1 > /dev/null
    return $?
}

# Var to help check and source the script only once
starx_bash_helper_function_loaded=true
