#!/bin/bash

# Helps build the find command
function build_find_arguments {
	local -n fcmd=$1
	local fcmd_path="$2"
	local fcmd_mindepth="${3:-1}"
	local fcmd_maxdepth="$4"
	local fcmd_type="$5"
	local fcmd_name="$6"
	local fcmd_regex="$7"

	# path
	fcmd+=("$fcmd_path")

	# -mindepth
	if [ ! -z "$fcmd_mindepth" ]; then
		fcmd+=(-mindepth $fcmd_mindepth)
	fi

	# -maxdepth
	if [ ! -z "$fcmd_maxdepth" ]; then
		fcmd+=(-maxdepth $fcmd_maxdepth)
	fi

	# -type
	if [ ! -z "$fcmd_type" ]; then
		fcmd+=(-type $fcmd_type)
	fi

	# -name
	if [ ! -z "$fcmd_name" ]; then
		fcmd+=(-name "$fcmd_name")
	fi

	# -regex
	if [ ! -z "$fcmd_regex" ]; then
		fcmd+=(-regex "$fcmd_regex")
	fi
}

function get_find_result_as_array {
	local local_find_results="$1"
	find_result=()
	# loop through the multiline value
	while read -r loop_find_result; do
		find_result+=("$loop_find_result")
	done <<< "$local_find_results"
}

function get_find_args_result {
	local find_args="$1"
	local filter_type="$2"
	find_result=()

	local local_find_results=$(find "${find_args[@]}")
	if [ "$filter_type" == "dir" ]; then
		local_find_results=$(echo "$local_find_results" | tac)
	fi

	while read -r loop_find_result; do
		find_result+=("$loop_find_result")
	done <<< "$local_find_results"
}

function get_find_result {
	local find_cmd="$1"
	local filter_type="$2"
	find_result=()

	local_find_results=`$find_cmd`
	while read -r loop_find_result; do
		find_result+=("$loop_find_result")
	done <<< "$local_find_results"
}

# Var to help check and source the script only once
starx_bash_helper_find_loaded=true
