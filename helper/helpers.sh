#!/bin/bash

# Collect all the args
argc=$#
argv=($@)


# Get(Identify) the machine the script is running on
#
get_machine() {
	# Get the machine information
	local machine=''
	local machine_linux='Linux'
	local machine_mac='Mac'
	local machine_cygwin='Cygwin'
	local machine_mingw='MinGw'
	local machine_unknown="UNKNOWN:${unameOut}"

	unameOut="$(uname -s)"
	case "${unameOut}" in
	    Linux*)     machine=$machine_linux;;
	    Darwin*)    machine=$machine_mac;;
	    CYGWIN*)    machine=$machine_cygwin;;
	    MINGW*)     machine=$machine_mingw;;
	    *)          machine=$machine_unknown;;
	esac

	echo "$machine"
}

# Returns full home path
#
get_home_path() {
	local tilde=~
	eval full_home_path=$tilde
	echo "$full_home_path"
}

# Get the path of the running script
#
get_script_path() {
	echo "$( cd "$(dirname "$0")" ; pwd -P )"
}

# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
# Reference: All major Dockerfiles
file_env() {
  local var="$1"
  local fileVar="${var}_FILE"
  local def="${2:-}"
  if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
    echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
    exit 1
  fi
  local val="$def"
  if [ "${!var:-}" ]; then
    val="${!var}"
  elif [ "${!fileVar:-}" ]; then
    val="$(< "${!fileVar}")"
  fi
  export "$var"="$val"
  unset "$fileVar"
}

# Clear the history and screen
# Reference: https://askubuntu.com/a/192001
# Reference: https://stackoverflow.com/a/5367075
cls() {
	# Clear the history & screen
	cat /dev/null > ~/.bash_history && history -c && printf "\033c"	
}

# Deletes something based on a mode
mdel() {
	local target="$1"
	local mode="${2:-rm}"
	local cmd="rm"

	# Resolve dependencies & decide command to use
	case "$mode" in
		secure|fast|srm)
			# check if srm exists first
			# add the stderr to stdout
			# redirect the output to /dev/null
			command -v srm 2>&1 > /dev/null
			# Check the exit code to know if command exists
			if [ $? -eq 0 ]; then
				cmd="srm"
			fi
			;;
	esac

	case "$cmd" in
		srm)
			# if "fast" is selected. set the options
			if [ "$mode" == "fast" ]; then
				cmd="$cmd -f"
			fi

			if [ -d "$target" ]; then
				cmd="$cmd -r"
			fi
			$cmd "$target"
			;;
		rm|*)
			if [ -f "$target" ]; then
				$cmd -f "$target"
			elif [ -d "$target" ]; then
				$cmd -rf "$target"
			fi
			;;
	esac
}

# Var to help check and source the script only once
starx_bash_helper_helpers_loaded=true