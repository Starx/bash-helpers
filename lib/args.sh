#!/bin/bash

function convert_option_args_to_variables {
	declare -a unnamed_args
	local arg_shift_position=0
	while [ "$#" -gt $arg_shift_position ]; do
		echo $arg_shift_position
		# Dynamic variables based on arg_shift_position
		local id_var=$((arg_shift_position+1))
		local value_var=$((arg_shift_position+2))
		local arg_id=${!id_var}
		local arg_value=${!value_var}

		case "$arg_id" in
			--*)
echo "double"
			# Find and replace first instance
			arg_name="${arg_id/--/}"
			;;
		-*)
echo "single"
			# Find and replace first instance
			arg_name="${arg_id/-/}"
			arg_value=true
			;;
		*)
			# Collect the unnamed arguments so that we can reset the arguments later
			unnamed_args+=($arg_id)
			shift
			continue;
			;;
		esac

		# Shift the arguments as per required
		case "$arg_id" in
	  		--*)
				shift
				shift
				;;
			-*)
	  			shift
				;;
		esac

		# Since Bash 4.3 (released 2014), the declare builtin has an option -n
		# for creating a variable which is a “name reference” to another variable
		declare -n ref="$arg_name"
		# Assigning input with printf
		# Ref: https://stackoverflow.com/a/55331060
		printf -v "$arg_name" '%s' $arg_value

	done

	# get length/count of an array
	local unnamed_args_length=${#unnamed_args[@]}
	log "debug" "$FUNCNAME: Unnamed arguments: $unnamed_args_length"
	# Use the remaining unnamed_args and create a new argument array
	set -- "${@:1:$unnamed_args_length}" "${unnamed_args[@]}"
}

# Var to help check and source the script only once
starx_bash_lib_args_loaded=true

