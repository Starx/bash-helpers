#!/bin/bash

# Setup repositories for the dev environment
function s2_env_dev_env_repo_setup {
	# Setup the repo
	mkdir -p $DEV_ENV_PATH
	cd $DEV_ENV_PATH
	git init
	git remote add origin git@github.com:starx/DevEnv.git
	git remote add https https://github.com/starx/DevEnv.git
}

function s2_env_dev_env_repo_pull {
	cd $DEV_ENV_PATH
	git pull origin master
}

# Pull repositories for the dev environment
function s2_env_dev_env_custom_repo_pull {
	cd $DEV_ENV_PATH/custom/$S2_ENV
	git pull origin master
}


# Configure the SSH Key for the bitbucket
function s2_env_config_ssh {
	execute_if_function_exists s2_env_${S2_ENV}_ssh_copy_keys
	# execute_if_function_exists s2_env_${S2_ENV}_ssh_link_keys

	# Add the bitbucket key
	# ssh-add ~/.ssh/bitbucket
}

# Pull all the submodules and configure
# them
function s2_env_setup_submodules {
	DESTINATION=$1
	if [ ! -z $DESTINATION ]; then
		
		cd $DESTINATION
		# To pull all the submodules when the repo is first checked out
		# Source: https://stackoverflow.com/a/1032653/295264
		git submodule update --init --recursive

		# For each submodule checkout the master branch
		# so that when changes are made, the HEAD is not
		# in an detached state already
		git submodule foreach --recursive "(git checkout master)"		
	else
		echo "Please specify a destination"
	fi
}

# Pull all the submodules and configure
# them
function s2_env_update_submodules {
	DESTINATION=$1
	if [ ! -z $DESTINATION ]; then
		
		cd $DESTINATION
		# For each submodule pull the latest changes from master master branch
		git submodule foreach --recursive "(git pull origin master)"
	else
		echo "Please specify a destination"
	fi
}

# Setup the custom script
# e.g.
# Link them, make them executable
function s2_env_setup_scripts {
	# Setup the scripts
	sudo chmod +x -R $DEV_ENV_PATH/scripts/*
	sudo ln -sfn $DEV_ENV_PATH/scripts/* /usr/local/bin/
}

# Setup the Chef
#
function s2_env_setup_chef {
	cd $DEV_ENV_PATH/chef

	# User berksfile to download coookbooks
	# and add them to a required path
	berks vendor berks-cookbooks/
	rm -rf $DEV_ENV_PATH/chef/berks-cookbooks/SEnv
}