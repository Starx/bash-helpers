#!/bin/bash

declare -A sources_loaded
function source_once {
	local file_path="$1"

	if [[ ! -f $file_path ]]; then
		return 1;
	fi

	NAMING_KEY=${naming_table[$CATEGORY]}

	script_hash=$(echo "${file_path}" | md5sum | awk '{print $1}')
	log "verbose" "$FUNCNAME File hash: $script_hash"
	# check if element / item does not exists in array
	if [ ! -v sources_loaded[$script_hash] ]; then
		log "debug" "$FUNCNAME Sourcing $file_path"
		source "$file_path"
		source_rc=$?
		log "debug" "$FUNCNAME Sourcing rc: $source_rc"
		sources_loaded[$script_hash]="$file_path"

		return $source_rc
	else
		log "verbose3" "$FUNCNAME Already sourced. Skipping."
		return 2;
	fi
	return 3;
}

# Var to help check and source the script only once
starx_bash_lib_include_loaded=true
