#!/bin/bash

function abort_if_variables_not_defined {
	# store arguments in a special array 
	args=("$@")
	# join array item by commna
	args_string=$(IFS=, ; echo "${args[*]}")

	declare -a not_defined
	# loop over the elements
	for var_name in "$@"
	do
		echo $var_name
		# Since Bash 4.3 (released 2014), the declare builtin has an option -n
		# for creating a variable which is a “name reference” to another variable
		declare -n ref="$var_name"

		# check for non-null/non-zero string variable
		# check if variable is defined
		if [ ! -n "$ref" ]; then
			not_defined+=("$var_name")
		fi
	done

	# count array and if greater than 0 then
	if [ "${#not_defined[@]}" -gt 0 ]; then
		# join array item by commna
		args_string=$(IFS=, ; echo "${not_defined[*]}")
		log "fatal" "Following variables are required: $args_string"
		exit 1
	fi
	return 0
}

# Var to help check and source the script only once
starx_bash_lib_variables_loaded=true
