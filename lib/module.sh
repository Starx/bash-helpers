#!/bin/bash

# Helper function to find and trigger partials scripts if exits
trigger_action_using_custom_module() {
	local module=$1
	if [ -f $CUSTOM_MODULES_DIR/$module.sh ]; then
		source "$CUSTOM_MODULES_DIR/$module.sh"
	else
		echo -e "${RED}Custom module not found.${NC}"
		echo -e "This action ${BYellow}'${ACTION}'${NC} has to be implemented on custom environment module: ${BYellow}${CUSTOM_MODULES_DIR}/${MODULE}${NC}"
		exit 1;
	fi
}
# Helper function to find and trigger partials scripts if exits
trigger_action_in_custom_module() {
	local module=$1
	# Override the action from what was passed.
	ACTION=$2
	trigger_action_using_custom_module "$@"
}

# Var to help check and source the script only once
starx_bash_lib_module_loaded=true
