#!/bin/bash

# Function start_ssh_agent
#
# Starts another ssh agent
#
function start_ssh_agent {
    echo "Starting new SSH Agent"
    (umask 066; ssh-agent > ~/.ssh-agent)
    # Start the agent
    eval "$(<~/.ssh-agent)" >/dev/null
    ssh-add
}

# Function recover_or_start_ssh_agent
#
# Attempts to recover the ssh agent from local file or starts another
#
function recover_or_start_ssh_agent {
	echo "Trying to recover from ~/.ssh-agent"

    # Check if ~/.ssh-agent file exists and 
    if [ -f ~/.ssh-agent ]; then
    	# Execute the contents if true
		eval "$(<~/.ssh-agent)" >/dev/null

	    # Recheck to see if authenticationnot agent can be connected
	    ssh-add -l > /dev/null 2>&1

	    # If still not, agent was not previously started
	    if [ "$?" == 2 ]; then
	        echo "SSH Agent could not be recovered."
	        start_ssh_agent
	    else
	        echo "SSH Agent Recovered."
	    fi
    else
    	echo "No ~/.ssh-agent file"
    	start_ssh_agent
	fi
}

function s2_ssh_create_auto_add_key_script {
	local script_file="/usr/local/bin/ssh_auto_add_key"
	echo -e '#!/bin/sh\necho $SSH_PASS' | sudo tee "$script_file" > /dev/null
	sudo chmod +x "$script_file"
}

function s2_ssh_get_cipher {
	local key_file="$1"
	echo $key_file | sed -r 's/^.*ssh_host_([^_]+)_key\.pub$/\1/'| tr '[a-z]' '[A-Z]'
}

function s2_ssh_get_md5_fingerprint {
	local key_file="$1"
	ssh-keygen -l -f "$key_file" -E md5 | awk '{print $2}' | cut -d":" -f 2-
}

function s2_ssh_get_sha1_fingerprint {
	local key_file="$1"
	# Old way
	# awk '{print $2}' "$key_file" | base64 -d | sha256sum -b | awk '{print $1}' | xxd -r -p | base64
	ssh-keygen -l -f "$key_file" -E sha256 | awk '{print $2}' | cut -d":" -f 2-
}

function s2_ssh_check_if_key_loaded {
	local key_file="$1"
	if [[ ! -f $key_file ]]; then
		log fatal "Not a valid key file. Aborting."
		return 2;
	fi
	key_file_md5_hash=$(s2_ssh_get_md5_fingerprint "$key_file")
	log debug "$key_file: MD5 Hash: $key_file_md5_hash"

	# Loop through results of command
	while read -r key_line; do
		local loaded_key_file=$(echo "$key_line"| awk '{print $3}')
		if [[ -f $loaded_key_file ]]; then
			log debug "$loaded_key_file"
			local loaded_key_file_md5_hash=$(s2_ssh_get_md5_fingerprint "$loaded_key_file")
			log debug "$loaded_key_file: MD5 Hash: $loaded_key_file_md5_hash"
			if [ "$loaded_key_file_md5_hash" == "$key_file_md5_hash" ]; then
				return 0;
			else 
				log debug "$loaded_key_file: Not matched."
			fi
		fi

	done <<< $(ssh-add -L)
	return 1;
}

function s2_ssh_load_if_not_loaded {
	local key_file="$1"
	s2_ssh_check_if_key_loaded "$key_file"
	if [ $? -eq 1 ]; then
		ssh-add "$key_file"
	fi
}

function s2_ssh_mass_load_keys {
	local key_path="${1:-~/.ssh}"
	log info "Reading from $key_path"

	# Make sure the auto add key script is present
	s2_ssh_create_auto_add_key_script

	# Read password
	echo -n Passphrase: 
	read -s passphrase

	echo -e "\n\n"
	find -L "$key_path" -type f -not -name *.pub 
	
	echo -e "\n\n"
	confirm "Will attempt to load the above keys. Continue?"

	if $confirmed; then
		find -L "$key_path" -type f -not -name *.pub -exec bash -c 'SSH_PASS="$1" SSH_ASKPASS="/usr/local/bin/ssh_auto_add_key" DISPLAY=1 ssh-add "$2" < /dev/null' - "$passphrase" '{}' \;
	fi
}

# Var to help check and source the script only once
starx_bash_lib_ssh_loaded=true
