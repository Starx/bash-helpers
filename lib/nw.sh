#!/bin/bash

# Change the network temporarily
#
# $1: Network UUID
#
function s2_nw_change_network {
	NID=$1
	if [ -z $C_CID ]; then
		echo "No active connection."
	fi

	# Use the connection, if available
	nmcli c up $NID > /dev/null 2>&1

	# If the command executed with error &
	# If we have a active connection
	# Assume connection as changed
	if [ $? -eq 0 ] && [ ! -z $C_CID ]; then
		C_CHANGED=true
	else
		C_CHANGED=false
	fi
}

# Revert the connection
#
function s2_nw_revert_connection {
	# If the connection was changed, revert to previous connection
	if $C_CHANGED; then
		echo 
		echo "Reverting connection."
		s2_nw_change_network $C_CID
	fi	
}
