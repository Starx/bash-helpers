#!/bin/bash

function s2_bash_add_env_variable() {
	local var_name="$1"
	local var_value="$2"
	local env_line='export '$var_name'="'$var_value'"'
	grep -q -F "$env_line" ~/.bash_profile || echo "$env_line" >> ~/.bash_profile
	grep -q -F "$env_line" ~/.bashrc || echo "$env_line" >> ~/.bashrc
}

# Var to help check and source the script only once
starx_bash_lib_bash_loaded=true
