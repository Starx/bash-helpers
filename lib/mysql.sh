#!/bin/bash

function s2_mysql_ask_password_if_not_provided {
	if [ -z "$db_password" ]; then
		read_input_if_not_defined "db_password" "Db password" false true
	fi	
}

function s2_mysql_get_db {
	local db_user="${1:-root}"
	local db_password="$2"
	local dbs="$3"

	s2_mysql_ask_password_if_not_provided

	if [ -z "$dbs" ]; then
		log warn "Dbs not provided. Getting"
		dbs=$(s2_mysql_get_all_dbs "$db_user" "$db_password")

		if [ ! $? -eq 0 ]; then
			log fatal "Can't get the list of dbs. Aborting"
			exit 1;
		fi
	else
		# Explode array using a separator
		explode "$4" ","
		dbs="${exploded_array[@]}"	
	fi

	dump_filename="${4:-dump_$1_$(date +%s).sql}"

	read -r -d '' cmd << EOF
		export MYSQL_PWD="$db_password"
		mysqldump --user=$db_user --add-drop-database --databases $dbs> /var/dumps/$dump_filename
		gzip /var/dumps/$dump_filename
EOF

	execute "$cmd"
	if [ $? -eq 0 ]; then
		execute "scp -r ls_stack_db2:/var/dumps/$dump_filename.gz ."

		if [ $? -eq 0 ]; then
			echo "Latest dump should be saved in $dump_filename"

			execute "rm -f /var/dumps/$dump_filename"			
		fi
	fi

	echo
}


function s2_mysql_get_all_dbs {
	local db_user="${1:-root}"
	local db_password="$2"

	s2_mysql_ask_password_if_not_provided

	read -r -d '' cmd << EOF
		export MYSQL_PWD="$db_password"
		mysql --user=$db_user -e 'show databases' \
			-s --skip-column-names \
			| grep -vi -E "information_schema|performance_schema|mysql|sys"
EOF

	execute "$cmd"
}

function s2_mysql_delete_all_dbs {
	local db_user="${1:-root}"
	local db_password="$2"

	s2_mysql_ask_password_if_not_provided

	read -r -d '' cmd << EOF
		export MYSQL_PWD="$db_password"
		mysql --user=$db_user -e "show databases" \
			| grep -v Database \
            | grep -v mysql | grep -v information_schema | grep -v performance_schema | grep -v sys \
            | gawk '{print "drop database \`" \$1 "\`;select sleep(0.1);"}' \
            | mysql --user=$db_user
EOF

	execute "$cmd"
}


function s2_mysql_query_dbs {
	local db_user="${1:-root}"
	local db_password="$2"
	local dbs="$3"

	s2_mysql_ask_password_if_not_provided

	if [ -z "$dbs" ]; then
		log warn "Dbs not provided. Getting"
		dbs=$(s2_mysql_get_all_dbs "$db_user" "$db_password")

		if [ ! $? -eq 0 ]; then
			log fatal "Can't get the list of dbs. Aborting"
			exit 1;
		fi
	else
		# Explode array using a separator
		explode "$dbs" ","
		dbs="${exploded_array[@]}"	
	fi
		
	# Read sql		
	echo "Paste in the SQL and end with a blank line:"
	read_input_if_not_defined "sql" "sql> " "" true
		
	# Escape sql
	# Find and replace all instances
	sql="${sql//\`/\\\`}"

	# Loop in the array
	for db in $dbs; do

		log info "Running on $db"
		log info

		read -r -d '' cmd << EOF
export MYSQL_PWD="$db_password"
mysql --user=$db_user $db <<- EOS
	$sql
	EOS
EOF
		execute "$cmd"
		log info "Exit code: $?"
		done
}

function s2_mysql_extract_table_from_server_backup {
	source_once "$LIB_DIR/args.sh"
	convert_option_args_to_variables "${@}"
	abort_if_variables_not_defined \
		db_name tbl_name \
		backup_name backup_file_name \
		backup_name backup_file_name \
		local_backup_path remote_backup_path 

	local local_backup_file_path="$local_backup_path/$backup_file_name"
	local remote_backup_file_path="$remote_backup_path/$backup_file_name"

	if [ -f "$local_backup_file_path" ]; then
		confirm "File already exists locally. Do you want to download again?"

		if $confirmed; then
			scp ls_stack_db2:$backup_file_path "$local_backup_path/"
		fi
	fi

	cd "$local_backup_path"

	read_input archive_pass "Archive password (if protected)" "" false true

	# Decrypt the archive
	read -r -d '' cmd << EOF
	cd "$local_backup_path"
	export ARCHIVE_PASS="u)T.|BWS#K42kP."
	7za x -y -p"\$ARCHIVE_PASS" "$backup_file_name" \
		"mysqldump-sutherland-db-vm-02-${db_name}-${backup_name}.gz"
EOF
	execute "$cmd"

	log info "Extracting the archive"	
	read -r -d '' cmd << EOF
	cd "$local_backup_path"
	gunzip "mysqldump-sutherland-db-vm-02-${db_name}-${backup_name}.gz"
EOF
	execute "$cmd"

	log info "Finding line number of table"
	read -r -d '' cmd << EOF
	cd "$local_backup_path"
	grep -n "INSERT INTO \\\`${tbl_name}\\\`" \
		"mysqldump-sutherland-db-vm-02-${db_name}-${backup_name}" \
		| cut -c -150
EOF
	execute "$cmd"

	# Show the grep result and ask to input the line range. 
	read_input line_range "Check the output above and provide the lines range to extract"

	if [[ -z $line_range ]]; then
		log fatal "Range not provided. Aborting."
		exit 1;
	fi

	log info "Extract the range ${line_range}p"
	read -r -d '' cmd << EOF
	cd "$local_backup_path"
	sed -n ${line_range}p \
		"mysqldump-sutherland-db-vm-02-${db_name}-${backup_name}" 
EOF
	local local_extracted_file="./${db_name}_${tbl_name}_${backup_name}.sql"
	execute "$cmd" > "$local_extracted_file"

	if [[ ! -s $local_extracted_file ]]; then
		log error "Extraction seems to have not succeeded."
		exit 1;
	fi

	ls -lah ./${db_name}_${tbl_name}_${backup_name}.sql	
	confirm "Extract complete. Please check above. Do you want to archive it?"

	if $confirmed; then
		gzip "./${db_name}_${tbl_name}_${backup_name}.sql"
	fi

	return 0;
}


function s2_mysql_get_var {
	local db_user="${1:-root}"
	local db_password="$2"
	local sql="$3"

	if [ -z "$db_password" ]; then
		read_input_if_not_defined "db_password" "Db password" "" false true
	fi

		read -r -d '' cmd << EOF
export MYSQL_PWD="$db_password"
mysql --user=$db_user -s --skip-column-names <<- EOS
	$sql
	EOS
EOF
	response=$(execute "$cmd")
	echo "$response"
	return $?
}

function s2_mysql_get_single_column {
	local db_user="${1:-root}"
	local db_password="$2"
	local sql="$3"
	result_var=

	echo $(s2_mysql_get_var "$db_user" "$db_password" "$sql")
	return $?
}

function s2_mysql_get_status_var {
	local db_user="${1:-root}"
	local db_password="$2"
	local var_name="$3"
	local global_var=${4:-true}

	if [ -z "$db_password" ]; then
		read_input_if_not_defined "db_password" "Db password" "" false true
	fi

	read -r -d '' cmd << EOF
		export MYSQL_PWD="$db_password"
		mysql --user=$db_user -e 'show global status like "%$var_name%"' \
			-s --skip-column-names
EOF
	response=$(execute "$cmd")
	echo "$response"
	return $?
}


function s2_mysql_get_single_status_var {
	local db_user="${1:-root}"
	local db_password="$2"
	local var_name="$3"
	local global_var=${4:-true}
	result_var=

	response=$(s2_mysql_get_status_var "$db_user" "$db_password" "$var_name" "$global_var")
	echo $(echo "$response") | cut -d " " -f2
	return $?
}

function s2_mysql_get_multi_status_var {
	local db_user="${1:-root}"
	local db_password="$2"
	local var_name="$3"
	local global_var=${4:-true}
	result_var=

	response=$(s2_mysql_get_status_var "$db_user" "$db_password" "$var_name" "$global_var")
	result_var=`echo $(echo "$response") | cut -d " " -f2`
	return $?
}

function s2_mysql_get_total_tables {
	local db_user="${1:-root}"
	local db_password="$2"

	echo $(s2_mysql_get_single_column "$db_user" "$db_password" \
		"SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE'")
	return $?
}
	

function s2_mysql_optimise_check_key_buffer_size {
	local db_password="$2"
	if [ -z "$db_password" ]; then
		read_input_if_not_defined "db_password" "Db password" "" false true
	fi

	local Uptime
	Uptime=$(s2_mysql_get_single_status_var "$1" "$db_password" "Uptime")
	echo "Uptime: $Uptime"
	
	local Key_read_requests
	Key_read_requests=$(s2_mysql_get_single_status_var "$1" "$db_password" "Key_read_requests")
	echo "Key_read_requests: $Key_read_requests"

	local Opened_tables
	Opened_tables=$(s2_mysql_get_single_status_var "$1" "$db_password" "Opened_tables")
	echo "Opened_tables: $Opened_tables"

	local Threads_connected
	Threads_connected=$(s2_mysql_get_single_status_var "$1" "$db_password" "Threads_connected")
	echo "Threads_connected: $Threads_connected"

	ratio=$(expr $Opened_tables / $Uptime)
	if (( $ratio > 2 )); then
		echo "Opened_tables / Uptime is > 2"
	fi

	# total_tables=$(s2_mysql_get_total_tables "$1" "$db_password")
	total_tables=328408
	echo $total_tables

	echo "Table_open_cache = total_tables*Threads_connected"
	echo $(expr $total_tables / $Threads_connected)	
}

# Var to help check and source the script only once
starx_bash_lib_mysql_loaded=true
