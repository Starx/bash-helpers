#!/bin/bash


function get_knife_cmd {
	knife_cmd=(docker run --rm -it -v $DEV_ENV_PATH:/env --workdir=/env/chef -e CHEF_ENV=$S2_ENV -e CHEF_ENV_ORG=$CHEF_ENV_ORG johnbarney/docker-knife:16)
}

function knife {
	get_knife_cmd
}

function s2_chef_empty_databag_folder {
	log debug 
	log debug "Emptying databag folder"
	# Delete databags content
	rm -rf $DEV_PATH/env/chef/data_bags/*
	return $?
}

function s2_chef_copy_machine_data_bag_to_databag_folder {
	local machine_name="$1"
	local project_admin="$2"
	log debug 
	log debug "Copy machine_data_bag folder"
	# Copy data bags
	cp -R $CUSTOM_DIR/chef/machine_data_bags/$machine_name/* $DEV_PATH/env/chef/data_bags/

	return $?
}

function s2_chef_ssh_config_prefx {
	local machine_name="$1"
	local project_admin="$2"
	echo "$project_admin"

	return $?
}

function s2_chef_get_ssh_host {
	local machine_name="$1"
	local project_admin="$2"
	ssh_config_prefix=$(s2_chef_ssh_config_prefx "$machine_name" "$project_admin")
	echo "${ssh_config_prefix}_${machine_name}"

	return $?
}

function s2_chef_get_prod_chef_role {
	local machine_name="$1"
	local project_admin="$2"
	ssh_config_prefix=$(s2_chef_ssh_config_prefx "$machine_name" "$project_admin")
	echo "docker_${machine_name}"

	return $?
}

function s2_chef_make_config_folder_on_prod {
	local machine_host="$1"
	local project_admin="$2"

	# Make the config folder on the prod if does not exits
	log debug 
	log debug "Creating config folder on the server"
	ssh -t $project_admin@$machine_host 'sudo mkdir -p /var/dev/env/config && sudo chown '$project_admin':'$project_admin' /var/dev/env/config'

	return $?
}

function s2_chef_upload_prod_config {
	local machine_host="$1"
	local project_admin="$2"
	# Upload the config
	log debug 
	log debug "Copying config folder to the server"
	rsync -av $CUSTOM_DIR/config/$machine_name/* $project_admin@$machine_host:/var/dev/env/config/	
	echo $?
	return $?	
}

function s2_chef_set_run_list {
	local machine_name="$1"
	local project_admin="$2"
	local chef_role=$(s2_chef_get_prod_chef_role "$machine_name" "$project_admin")
	
	# Go to the chef folder and update the run list
	cd $DEV_ENV_PATH/chef
	log debug 
	log debug "Setting run list for the server"
	"${knife_cmd[@]}" node run_list set $machine_name 'role['$chef_role']'

	return $?
}

function s2_chef_bootstrap_node {
	local machine_name="$1"
	local machine_ip="$2"
	local machine_host="$3"
	local project_admin="$4"
	local chef_role=$(s2_chef_get_prod_chef_role "$machine_name" "$project_admin")

	read_input_if_not_defined machine_ip

	"${knife_cmd[@]}" bootstrap "$machine_ip" \
		--node-name "$machine_name" \
		-U "$project_admin" \
		--sudo \
		--run-list "role[$chef_role]" \
		--environment prod \
		--yes

	return $?
}

function s2_chef_run_chef_client {
	local machine_host="$1"
	local project_admin="$2"
	
	echo ssh -t $project_admin@$machine_host 'sudo chef-client'	

	ssh -t $project_admin@$machine_host 'sudo chef-client'

	return $?
}

function s2_chef_update_all_policy {
	confirm "Do you want to delete previous databags?"
	if $confirmed; then

		log debug
		log debug "Deleting existing databags."

		databags=$("${knife_cmd[@]}" data bag list | tail -n +4 )
		for databag in $databags
		do
			databag=$(trim "$databag")
			if [ -z $databag ]; then
				continue
			fi

			log debug "Deleting data bag $databag"
			"${knife_cmd[@]}" data bag delete $databag --yes
		done
		s1=$?
	else
		s1=0
	fi

	# log debug 
	log debug "Updating all chef related items."

	log debug
	log debug "Uploading everything using dot."
	"${knife_cmd[@]}" upload .
	s2=$?

	# log debug
	log debug "Uploading everything using --all."
	"${knife_cmd[@]}" cookbook upload --all
	s3=$?
	
	# Check if a folder contains files
	files=$(shopt -s nullglob dotglob; echo roles/*.rb)
	if (( ${#files} ))
	then
		log debug
		log debug "Uploading roles from base."
		"${knife_cmd[@]}" role from file roles/*.rb
		s4=$?
	else
		s4=0
	fi
	
	log debug
	log debug "Uploading databags."
	"${knife_cmd[@]}" upload data_bags/
	s5=$?
	
	knife_custom_dir="$CUSTOM_DIR"
	knife_custom_dir="../custom/$S2_ENV"
	log debug
	log debug "Uploading roles from the custom folder \"$knife_custom_dir\"."
	"${knife_cmd[@]}" role from file $knife_custom_dir/chef/roles/*.rb
	s6=$?

	# combining exit codes ( OR ing exit codes )
	! (( $s1 | $s2 | $s3 | $s4 | $s5 | $s6 ))
	s_all=$?

	return $s_all
}

function s2_chef_prepare_prod {
	machine_name="$1"
	machine_host="$2"	
	project_admin="$3"
	mn="$machine_name"
	pa="$project_admin"

	"${knife_cmd[@]}" ssl check
	if [ ! $? -eq 0 ]; then
		log error "Couldn't connect to chef."
		exit 1;
	fi

	s2_chef_empty_databag_folder "$mn" "$pa"
	if [ ! $? -eq 0 ]; then
		log error "Couldn't empty databag folder"
		exit 1;
	fi

	s2_chef_copy_machine_data_bag_to_databag_folder "$mn" "$pa"
	if [ ! $? -eq 0 ]; then
		log error "Couldn't copy ${md}'s' 
		databags to databag folder"
		exit 1;
	fi

	s2_chef_make_config_folder_on_prod "$machine_host" "$pa"
	if [ ! $? -eq 0 ]; then
		log error "Couldn't make config folder on production"
		exit 1;
	fi

	s2_chef_upload_prod_config "$machine_host" "$pa"
	if [ ! $? -eq 0 ]; then
		log error "Couldn't upload config on production"
		exit 1;
	fi	
}


function s2_chef_provsion_prod {
	machine_host="$1"	
	project_admin="$2"
	s2_chef_run_chef_client "$machine_host" "$project_admin"		
	if [ ! $? -eq 0 ]; then
		log error "chef_client run failed. $mn"
		exit 1;
	fi
}