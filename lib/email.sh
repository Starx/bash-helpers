#!/bin/bash

function s2_send_plain_curl_email {
	local to="$1"	
	local from="$2"

	local subject="$3"
	local content="$4"
	local url="$5"
	local user="$6"

	local to_name="$7"
	local from_name="$8"

	# Compose email file
	local email_file=$(tempfile)

	if [ -z $to_name ]; then
		echo "From: <$from>" > $email_file
	else
		echo "From: ${from_name} <$from>" > $email_file
	fi

	if [ -z $to_name ]; then
		echo "To: <$to>" > $email_file
	else
		echo "To: ${to_name} <$to>" > $email_file
	fi

	echo "Subject: ${subject}" >> $email_file
	echo "Content-Type: text/plain; charset=\"utf8\"" >> $email_file
	echo >> $email_file
	echo "$content" >> $email_file

	cat $email_file
	echo $url
	echo $from
	echo $to
	echo $email_file
	echo $user

	curl \
		--url "$url" \
		--ssl-reqd \
		--mail-from "$from" \
		--mail-rcpt "$to" \
	  	--upload-file "$email_file" \
	  	--user "$user"

	rm -f $email_file
}

function s2_send_plain_curl_gmail {
	local to="$1"	
	local from="$2"
	local password="$3"

	local subject="$4"
	local content="$5"

	local to_name="$6"
	local from_name="$7"

	s2_send_plain_curl_email \
		"$to" "$from" \
		"$subject" "$content" \
		'smtps://smtp.gmail.com:465' \
		"$from:$password" \
		"$to_name" "$from_name"
}

function s2_send_failure_email {
	local to="$1"	
	local from="$2"
	local password="$3"

	s2_send_plain_curl_gmail \
		"$to" "$from" "$password"
		'Error backing up' \
		'Errors were encounterd backing up.'
}

function s2_send_hello_world_email {
	email_file=$(tempfile)
	# Compose message
	echo "From: Earthling <people@earth.com>" >> $email_file
	echo "To: Martians <people@mars.com>" >> $email_file
	echo "Subject: Helloooo world" >> $email_file
	echo "Content-Type: text/plain; charset=\"utf8\"" >> $email_file
	echo >> $email_file
	echo "Hi! :)" >> $email_file

	# Send the email
	curl \
		--url 'smtps://smtp.gmail.com:465' \
		--ssl-reqd \
		--mail-from 'people@earth.com' \
		--mail-rcpt 'people@mars.com' \
		--upload-file $email_file \
		--user 'people@earth.com:supersecretpassword'

	# Cleanup
	rm -f $email_file
}

# Var to help check and source the script only once
starx_bash_lib_email_loaded=true