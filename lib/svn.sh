#!/bin/bash


#
# Source: https://gist.github.com/rmpel/27561cd32fc01a06d89082dca0a4297d
#
function s2_svn_get_all_authors {
    svn log -q | awk -F '|' '/^r/ {sub("^ ", "", $2); sub(" $", "", $2); print $2" = "$2" <"$2"@example.com>"}' | sort -u 
}

# Var to help check and source the script only once
starx_bash_lib_svn_loaded=true

